package com.app.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.app.bean.Song;
import com.app.bean.User;

public class SapeMusicDAOImpl implements SapeMusicDAO {
	PreparedStatement ps;
	Connection connection;
	
	

	@Override
	public ArrayList<Song> getAllSongsFromDB() {
		ArrayList<Song> songList=new ArrayList<>();
		connection = ConnectionDAO.openConnection();
		
		try {
			
			ps=connection.prepareStatement("select * from Song");
		
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				int id = rs.getInt(1);
				String songname=rs.getString(2);
				String artist=rs.getString(2);
				String language=rs.getString(3);
				String movie=rs.getString(4);
				String genre=rs.getString(5);
				
				Song song=new Song();
				song.setId(id);
				song.setSongname(songname);
				song.setArtist(artist);
				song.setLanguage(language);
				song.setMovie(movie);
				song.setGenre(genre);
				songList.add(song);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//logger.error(e.getMessage());
		}finally{
			try{
				if(ps != null){
				if(connection !=null){
					ps.close();
					ConnectionDAO.closeConnection();
					//logger.info("database closed");
				}
			}
		}
			catch(Exception e){
				//logger.error(e.getMessage());
			}}
		
		return songList;
	
	}
	public ArrayList<Song> getAllSongsByGenreFromDB(String genre) {
		ArrayList<Song> songList=new ArrayList<Song>();
	       PreparedStatement statement=null;
			try{
				connection = ConnectionDAO.openConnection();
				statement=connection.prepareStatement("select * from song where  genre=?");
				statement.setString(1, genre);
				ResultSet rs=statement.executeQuery();
				while(rs.next()){
					int id=rs.getInt(1);
					String name=rs.getString(2);
					String artist=rs.getString(3);
					String language=rs.getString(4);
					String movie=rs.getString(5);
					String newgenre=rs.getString(6);
					
					Song song=new Song();
					song.setArtist(artist);
					song.setGenre(newgenre);
					song.setId(id);
					song.setLanguage(language);
					song.setSongname(name);
					song.setMovie(movie);
					
					
					songList.add(song);
				}
			}catch(Exception e){
				//logger.error(e.getMessage());
				e.printStackTrace();
			}finally {
				try {
					if (statement != null)
					   statement.close();
					ConnectionDAO.closeConnection();
					//logger.info("database closed");
				
				} catch (SQLException e) {
					//logger.error(e.getMessage());
				}
			}
			if(songList.size()==0)
			System.out.println("Genre not found");
			//logger.info("showing items by category");
			return songList;
	
	
	}




	@Override
	public ArrayList<Song> getAllSongsByMovieFromDB(String movie) {
		ArrayList<Song> songList=new ArrayList<Song>();
	       PreparedStatement statement=null;
			try{
				connection = ConnectionDAO.openConnection();
				statement=connection.prepareStatement("select * from song where  movie=?");
				statement.setString(1, movie);
				ResultSet rs=statement.executeQuery();
				while(rs.next()){
					int id=rs.getInt(1);
					String name=rs.getString(2);
					String artist=rs.getString(3);
					String language=rs.getString(4);
					String newmovie=rs.getString(5);
					String genre=rs.getString(6);
					
					Song song=new Song();
					song.setArtist(artist);
					song.setGenre(genre);
					song.setId(id);
					song.setLanguage(language);
					song.setSongname(name);
					song.setMovie(newmovie);
					
					
					songList.add(song);
				}
			}catch(Exception e){
				//logger.error(e.getMessage());
				e.printStackTrace();
			}finally {
				try {
					if (statement != null)
					   statement.close();
					ConnectionDAO.closeConnection();
					//logger.info("database closed");
				
				} catch (SQLException e) {
					//logger.error(e.getMessage());
				}
			}
			if(songList.size()==0)
			System.out.println("Movie not found");
			//logger.info("showing items by category");
			return songList;
	
	}
	@Override
	public ArrayList<Song> getAllSongsByLanguageFromDB(String language) {
		ArrayList<Song> songList = new ArrayList<Song>();
		PreparedStatement statement = null;
		try {
			connection = ConnectionDAO.openConnection();
			statement = connection.prepareStatement("select * from song where language=?");
			statement.setString(1, language);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String artist = rs.getString(3);
				String newlanguage = rs.getString(4);
				String movie = rs.getString(5);
				String genre = rs.getString(6);

				Song song = new Song();
				song.setArtist(artist);
				song.setGenre(genre);
				song.setId(id);
				song.setLanguage(newlanguage);
				song.setSongname(name);
				song.setMovie(movie);

				songList.add(song);
				System.out.println("Songs retrieved");
			}
		} catch (Exception e) {
			// logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				ConnectionDAO.closeConnection();
				// logger.info("database closed");

			} catch (SQLException e) {
				// logger.error(e.getMessage());
			}
		}
		if (songList.size() == 0)
			System.out.println("Language not found");
		// logger.info("showing items by category");
		return songList;

	}

	@Override
	public ArrayList<Song> getAllSongsByArtistFromDB(String artist) {
		ArrayList<Song> songList = new ArrayList<Song>();
		PreparedStatement statement = null;
		try {
			connection = ConnectionDAO.openConnection();
			statement = connection.prepareStatement("select * from song where  artist=?");
			statement.setString(1, artist);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String newartist = rs.getString(3);
				String language = rs.getString(4);
				String movie = rs.getString(5);
				String genre = rs.getString(6);

				Song song = new Song();
				song.setArtist(newartist);
				song.setGenre(genre);
				song.setId(id);
				song.setLanguage(language);
				song.setSongname(name);
				song.setMovie(movie);

				songList.add(song);
			}
		} catch (Exception e) {
			// logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				ConnectionDAO.closeConnection();
				// logger.info("database closed");

			} catch (SQLException e) {
				// logger.error(e.getMessage());
			}
		}
		if (songList.size() == 0)
			System.out.println("Artist not found");
		// logger.info("showing items by category");
		return songList;
	

	}
	@Override
	public void addUserDetailsToUserDB(User user) {
connection=ConnectionDAO.openConnection();
		
		try {
			ps=connection.prepareStatement("insert into Users values (?,?)");
			
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.execute();
			//logger.info("Inserted Sucessfully");
			
		} catch (SQLException e) {
			e.printStackTrace();
		//	logger.error(e.getMessage());
		}
		finally{
			if(ps!=null){
				try {
					ps.close();
					ConnectionDAO.closeConnection();
	//				
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		}
	}


	@Override
	public void addSongToDB(Song song) {
connection=ConnectionDAO.openConnection();
		
		try {
			ps=connection.prepareStatement("insert into Song values (?,?,?,?,?,?)");
			ps.setInt(1,song.getId());
			ps.setString(2, song.getSongname());
			ps.setString(3, song.getArtist());
			ps.setString(4, song.getLanguage());
			ps.setString(5, song.getMovie());
			ps.setString(6, song.getGenre());
			
			ps.execute();
			//logger.info("Inserted Sucessfully");
			
		} catch (SQLException e) {
			e.printStackTrace();
		//	logger.error(e.getMessage());
		}
		finally{
			if(ps!=null){
				try {
					ps.close();
					ConnectionDAO.closeConnection();
	//				
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		}
	}



	@Override
	public void updateSongToDB(Song song) {
		// TODO Auto-generated method stub
		try {
			
			connection = ConnectionDAO.openConnection();
		//	logger.info("connection established");
			String updateQuery =  "update Song set id =?  where name =? ";
		
			ps= connection.prepareStatement(updateQuery);
				ps.setInt(1, song.getId());
				ps.setString(2, song.getSongname());
				ps.execute();
				//logger.info("updated the changes sucessfully");

				
			} catch (Exception e) {
			e.printStackTrace();
				//	logger.error(e.getMessage());
			}
			finally{
				try{
					if(ps != null){
					if(connection !=null){
						ps.close();
						ConnectionDAO.closeConnection();
					//	logger.info("database closed");
					}
				}
			}
				catch(Exception e){
				//	logger.error(e.getMessage());
				e.printStackTrace();
				}}}
			
			
		
		
	@Override
	public void deleteSongFromDB(Song song) {
	try {
			
			connection = ConnectionDAO.openConnection();
		//	logger.info("connection established");
			String deleteQuery =  "Delete from Song where name =? ";
			

			ps= connection.prepareStatement(deleteQuery);
			ps.setString(1,song.getSongname());
			
			ps.executeUpdate();
			
				
			} catch (Exception e) {
			e.printStackTrace();
				//	logger.error(e.getMessage());
			}
			finally{
				try{
					if(ps != null){
					if(connection !=null){
						ps.close();
						ConnectionDAO.closeConnection();
					//	logger.info("database closed");
					}
				}
			}
				catch(Exception e){
				//	logger.error(e.getMessage());
				e.printStackTrace();
				}}}
	
	@Override

	public boolean validateSapeMusicDAO(String username, String password) {
		boolean status=false;
		Connection con=ConnectionDAO.openConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement("select * from users where name=? and password=?");
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet rs=ps.executeQuery();
			status = rs.next();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				if(ps!=null)
				ps.close();
				ConnectionDAO.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(status){
			return true;
		}else{
			return false;
		}

	}


}
